//
//  AddQuoteViewController.h
//  Quote
//
//  Created by Timotin Vanea on 2/14/14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddQuoteViewController : UIViewController<UITextViewDelegate,UITableViewDataSource,UITextViewDelegate,UIAlertViewDelegate>
{
    __weak IBOutlet UITextView *textViewWithMyQuote;

    __weak IBOutlet UIView *viewWithText;

    __weak IBOutlet UIButton *add;
}
- (IBAction)addMyQuote:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableWithMyQuotes;

- (IBAction)editQuoteAction:(id)sender event:(id)event;
@end
