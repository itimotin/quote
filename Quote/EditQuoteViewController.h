//
//  EditQuoteViewController.h
//  Quote
//
//  Created by Timotin Vanea on 1/8/14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditQuoteViewController : UIViewController<UITextViewDelegate, UITextFieldDelegate> {

    IBOutlet UIButton *aproveButton;
    IBOutlet UITextView *textViewEdit;

    IBOutlet UIView *viewWithContent;
}

- (IBAction)aproveAction:(id)sender;
-(void)editThisQuote:(NSString*)quote citeBdy:(NSString*)cite;

@property (readwrite) NSString *quote;
@property (readwrite, retain) NSString *cite;
- (IBAction)fontSelectAction:(id)sender;

@end
