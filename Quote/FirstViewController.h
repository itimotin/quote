//
//  FirstViewController.h
//  Quote
//
//  Created by Timotin Vanea on 12/22/13.
//  Copyright (c) 2013 Timotin Vanea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstViewController : UIViewController<UIAlertViewDelegate> {

    __weak IBOutlet UIView *workingView;
    __weak IBOutlet UIButton *redactButton;
    IBOutlet UIButton *simpleShare;
    __weak IBOutlet UILabel *name;
    IBOutlet UIView *viewWithButtons;
    IBOutlet UIView *viewWithShareButtons;
    UIImage *imageToShareScreeenShot;
}

@property (weak, nonatomic) IBOutlet UITextView *quoteTextView;

- (IBAction)simpleShare:(id)sender;
- (IBAction)redactAction:(id)sender;
@property (readwrite) NSString *quote;
@property (readwrite, retain) NSString *cite;
- (IBAction)shareTo:(id)sender;
@end
