//
//  ReadViewController.m
//  Quote
//
//  Created by Admin on 03.02.14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

#import "ReadViewController.h"
#import "EditQuoteViewController.h"
#import "PreviewViewController.h"

#import "SHKFacebook.h"
#import "SHKVkontakte.h"
#import "SHKTwitter.h"
#import "SHKMail.h"
#import "SHKDropbox.h"
#import "SHKGooglePlus.h"
#import "SHKFlickr.h"
#import "SHKTumblr.h"
#import "SHK.h"
#import "SHKItem.h"
#import "SHKSharer.h"

#import <AssetsLibrary/AssetsLibrary.h>

@interface ReadViewController ()

@property (readwrite, nonatomic) IBOutlet UIButton *buttonReaded;
@property (readwrite, nonatomic) IBOutlet UIButton *buttonLiked;
@end

@implementation ReadViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!isIOS7orHigher()) {
        [textViewSelectedQuote addObserver:self forKeyPath:@"contentSize" options:(NSKeyValueObservingOptionNew) context:NULL];
    }
	// Do any additional setup after loading the view.
    [self addTextToTextView];
    
    [self arangementOnView];
     self.title = @"Read";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    [self colorAll];
    if (isIOS7orHigher()) {
        [self centerText];
    }else{
        [self textToAlighnVerticalyCenter];
    }
}

- (void)colorAll
{
    NSDictionary *colorDict = [dictColor getDictWithColorsForID:[[usrDef objectForKey:@"color"] integerValue]];
    self.view.backgroundColor = UIColorFromRGB([[colorDict objectForKey:BGColor] integerValue]);
    redactButton.layer.cornerRadius = simpleShare.layer.cornerRadius = 5;
    textViewSelectedQuote.backgroundColor = redactButton.backgroundColor = simpleShare.backgroundColor = UIColorFromRGB([[colorDict objectForKey:IMG_BGColor] integerValue]);
     labelWithNameCite.textColor = textViewSelectedQuote.textColor = viewWithText.backgroundColor = UIColorFromRGB([[colorDict objectForKey:TextColor] integerValue]);
    [redactButton setTitleColor:UIColorFromRGB([[colorDict objectForKey:TextColor] integerValue]) forState:UIControlStateNormal];
    [simpleShare setTitleColor:UIColorFromRGB([[colorDict objectForKey:TextColor] integerValue]) forState:UIControlStateNormal];
    
    if (!isIOS7orHigher()) {
        [[self navigationController] navigationBar].tintColor = UIColorFromRGB([[colorDict objectForKey:IMG_BGColor] integerValue]);
    }
 
//    name.textColor = quoteTextView.textColor = workingView.backgroundColor = UIColorFromRGB([[colorDict objectForKey:TextColor] integerValue]);
}


- (void)arangementOnView{
    
    if (isIOS7orHigher()) {
        if (IS_IPAD) {
            
        }else if (isFourInch){
            
        }else{
            
        }
        
    }else{
        
        if (IS_IPAD) {
            
        }else if (isFourInch){
            viewWithText.center = CGPointMake(WIDTH_DEVICE/2, WIDTH_DEVICE/2-15);
            viewWithButtons.center = CGPointMake(WIDTH_DEVICE/2, HEIGHT_DEVICE/2+95);
        }else{
            viewWithText.center = CGPointMake(WIDTH_DEVICE/2, WIDTH_DEVICE/2-15);
            viewWithButtons.center = CGPointMake(WIDTH_DEVICE/2, HEIGHT_DEVICE/2+95);
        }
        [textViewSelectedQuote removeObserver:self forKeyPath:@"contentSize"];
    }
    
}

- (void)textToAlighnVerticalyCenter {
    CGFloat offSet = ([textViewSelectedQuote bounds].size.height - [textViewSelectedQuote contentSize].height * [textViewSelectedQuote zoomScale])/2.0;
    offSet = offSet < 0.0 ? 0.0 : offSet;
    [textViewSelectedQuote setContentOffset:CGPointMake(0, -offSet) animated:NO];
}

- (void)centerText
{
    NSTextContainer *container = textViewSelectedQuote.textContainer;
    NSLayoutManager *layoutManager = container.layoutManager;
    
    CGRect textRect = [layoutManager usedRectForTextContainer:container];
    
    UIEdgeInsets inset = UIEdgeInsetsZero;
    inset.top = textViewSelectedQuote.bounds.size.height / 2 - textRect.size.height / 2;
    inset.left = textViewSelectedQuote.bounds.size.width / 2 - textRect.size.width / 2;
    textViewSelectedQuote.textContainerInset = inset;
}


-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    UITextView *tv = object;
    CGFloat topCorrect = ([tv bounds].size.height - [tv contentSize].height * [tv zoomScale])/2.0;
    topCorrect = ( topCorrect < 0.0 ? 0.0 : topCorrect );
    tv.contentOffset = (CGPoint){.x = 0, .y = -topCorrect};
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)readThisQuote:(id)sender {
    UIButton *btn = (UIButton*)sender;
    btn.selected = !btn.selected;
}

- (IBAction)actionSimpleSharing:(id)sender {
    self.buttonLiked.hidden =  YES;
    viewWithButtons.hidden = YES;
    viewWithShareButtons.hidden = YES;
    
    UIGraphicsBeginImageContextWithOptions(viewWithText.frame.size, NO, 0.0);
    [viewWithText.layer renderInContext:UIGraphicsGetCurrentContext()];
    imageToShareScreeenShot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    viewWithShareButtons.hidden = NO;
    viewWithButtons.hidden = NO;
    self.buttonLiked.hidden =  NO;
    [self showViewWithButtons];
}

- (IBAction)actAdvancedSharing:(id)sender {
    EditQuoteViewController* editViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"EditId"];
    editViewController.quote = self.quote;
    editViewController.cite = self.cite;
    [self.navigationController pushViewController:editViewController animated:YES];
}

- (IBAction)likeThisQuote:(id)sender {
    BOOL b;
    UIButton *btn = (UIButton*)sender;
    btn.selected = !btn.selected;
    if (btn.selected) {
        b = [storageDB addToFavoritesQuoteWithId:self.idQuote];
    }else{
        b = [storageDB removeFromFavoritesQuoteWithId:self.idQuote];
    }
    NSLog(@"SCRIEREA BD BOOL (%d)",b);
    
    if ([self.delegate respondsToSelector:@selector(wasLikeInReadView:)]) {
        [self.delegate wasLikeInReadView:btn.selected];
    }else{
        NSLog(@"nu raspunde Delegatul");
    }
}

- (void)addTextToTextView
{
    textViewSelectedQuote.text = self.quote;
//    textViewSelectedQuote.textAlignment = NSTextAlignmentCenter;
    [textViewSelectedQuote setFont:[UIFont fontWithName:FONT_DefoItalic size:((IS_IPAD)?25:17)]];
    labelWithNameCite.text = self.cite;
    if (self.liked.integerValue != 100) {
        self.buttonLiked.selected = (BOOL)self.liked.integerValue;
    }else
    {
        self.buttonLiked.hidden = YES;
        self.buttonReaded.hidden = YES;
    }
}

-(void)showViewWithButtons {
    viewWithShareButtons.alpha = 1;
    [UIView animateWithDuration:.5 animations:^{
        
        viewWithShareButtons.center = CGPointMake(viewWithShareButtons.frame.size.width/2, HEIGHT_DEVICE/2-((isIOS7orHigher())?30:65));
    } completion:^(BOOL finished){
    }];
}

- (void)hiddenViewWithButtons {
    //    [shareButton setImage:[UIImage imageNamed:@"share"]];
    [UIView animateWithDuration:.5 animations:^{
        viewWithShareButtons.center = CGPointMake(-viewWithShareButtons.frame.size.width/2, HEIGHT_DEVICE/2-((isIOS7orHigher())?30:65));
    } completion:^(BOOL finished){
        viewWithShareButtons.alpha = 0.0f;
    }];
}

- (IBAction)shareTo:(id)sender {
    [self hiddenViewWithButtons];
    NSInteger tag = ((UIButton*)sender).tag;
    SHKItem *item=[[SHKItem alloc]init];
    item.shareType=SHKShareTypeImage;
    item.title=@"Feed your brain";
    item.image = imageToShareScreeenShot;
    switch (tag) {
        case 1:
            //facebook
            [SHKFacebook shareItem:item];
            break;
        case 2:
            //library
            UIImageWriteToSavedPhotosAlbum(imageToShareScreeenShot, nil, nil, nil);
            break;
        case 3:
            //twit
            [SHKTwitter shareItem:item];
            break;
        case 4:
            //tumblr
            [SHKTumblr shareItem:item];
            break;
        case 5:
            //flickr
            [SHKFlickr shareItem:item];
            break;
        case 6:
            //mail
            [SHKMail shareItem:item];
            break;
        case 7:
            //vk
            [SHKVkontakte shareItem:item];
            break;
        case 8:
            //dropbox
            [SHKDropbox shareItem:item];
            break;
        case 9:
            //g+
//            [SHKGooglePlus shareItem:item];
            break;
            
            
        default:
            break;
    }
    
}

@end
