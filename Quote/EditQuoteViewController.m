//
//  EditQuoteViewController.m
//  Quote
//
//  Created by Timotin Vanea on 1/8/14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

#import "EditQuoteViewController.h"
#import "PhotoViewController.h"

@interface EditQuoteViewController (){
    BOOL textWasChanged;
    NSArray *arrayFonts;
    NSString *selectedFont;
}

@end

@implementation EditQuoteViewController
@synthesize quote=_quote;
@synthesize cite = _cite;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    if (!isIOS7orHigher()) {
        [textViewEdit addObserver:self forKeyPath:@"contentSize" options:(NSKeyValueObservingOptionNew) context:NULL];
    }
    [super viewDidLoad];
    
    arrayFonts = [NSArray arrayWithObjects:@"HelveticaNeue-LightItalic", @"Times New Roman", @"Marion-Bold", @"King Richard", @"Goodfish", @"Kenyan Coffee", nil];

    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    textViewEdit.delegate=self;
    
	[self addButtonToNavigationBar];
    [self editThisQuote:self.quote citeBdy:self.cite];

    // Do any additional setup after loading the view.
    textWasChanged = NO;
    [self arangementOnView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self colorAll];
    [self fontSelectAction:nil];
    if (!isIOS7orHigher()) {
        [self textToAlighnVerticalyCenter];
    }else{
//        [self centerText];
//        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
//            self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    CGRect line = [textView caretRectForPosition:
                   textView.selectedTextRange.start];
    CGFloat overflow = line.origin.y + line.size.height
    - ( textView.contentOffset.y + textView.bounds.size.height
       - textView.contentInset.bottom - textView.contentInset.top );
    if ( overflow > 0 ) {
        // We are at the bottom of the visible text and introduced a line feed, scroll down (iOS 7 does not do it)
        // Scroll caret to visible area
        CGPoint offset = textView.contentOffset;
        offset.y += overflow + 7; // leave 7 pixels margin
        // Cannot animate with setContentOffset:animated: or caret will not appear
        [UIView animateWithDuration:.2 animations:^{
            [textView setContentOffset:offset];
        }];
    }
}

- (void)colorAll
{
    NSDictionary *colorDict = [dictColor getDictWithColorsForID:[[usrDef objectForKey:@"color"] integerValue]];
    self.view.backgroundColor = UIColorFromRGB([[colorDict objectForKey:BGColor] integerValue]);
    
    
    textViewEdit.backgroundColor = UIColorFromRGB([[colorDict objectForKey:IMG_BGColor] integerValue]);
    
    if (!isIOS7orHigher()) {
        [[self navigationController] navigationBar].tintColor = UIColorFromRGB([[colorDict objectForKey:IMG_BGColor] integerValue]);
    }
    
    for (NSInteger i = 1; i <= 6; i++) {
        UIButton *btn = (UIButton*)[self.view viewWithTag:i];
        [btn setTitleColor:UIColorFromRGB([[colorDict objectForKey:TextColor] integerValue]) forState:UIControlStateSelected];
        [btn setTitleColor:UIColorFromRGB([[colorDict objectForKey:TextColor] integerValue]) forState:UIControlStateNormal];
        btn.backgroundColor = UIColorFromRGB([[colorDict objectForKey:IMG_BGColor] integerValue]);
    }
    
    textViewEdit.textColor = UIColorFromRGB([[colorDict objectForKey:TextColor] integerValue]);
}

- (void)arangementOnView{
    
    if (isIOS7orHigher()) {
        if (IS_IPAD) {
            
        }else if (isFourInch){
            
        }else{
            
        }
        
    }else{
        
        if (IS_IPAD) {
            
        }else if (isFourInch){
            
        }else{
            viewWithContent.center = CGPointMake(WIDTH_DEVICE/2, 185);
        }
            [textViewEdit removeObserver:self forKeyPath:@"contentSize"];
    }
}



- (void)addButtonToNavigationBar{
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Image" style:UIBarButtonItemStylePlain target:self action:@selector(goToPictureViewController:)];
    self.navigationItem.rightBarButtonItem = anotherButton;
    self.title = @"Edit";
}

-(IBAction)goToPictureViewController:(id)sender{
    PhotoViewController* photoiewController = [self.storyboard instantiateViewControllerWithIdentifier:@"photoId"];
    photoiewController.editeCite = self.cite;
    photoiewController.editedQuote = textViewEdit.text;
    photoiewController.strFont = selectedFont;
    
    if (textWasChanged) {
        photoiewController.strNameCite = @"ME";
    }else{
        photoiewController.strNameCite = self.cite;
        
    }
    [self.navigationController pushViewController:photoiewController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)editThisQuote:(NSString *)quote citeBdy:(NSString *)cite{
    textViewEdit.text = quote;
}

#pragma mark - TEXTVIEW Delegate
- (void)textViewDidBeginEditing:(UITextView *)textView{
    aproveButton.alpha = 0.0;
    aproveButton.hidden = NO;
    [UIView animateWithDuration:0.3 animations:^{
        aproveButton.alpha = 1.0;
    }];
    
}

#pragma mark - Accept Redacted Text
- (IBAction)aproveAction:(id)sender {
    [textViewEdit resignFirstResponder];
    aproveButton.hidden = YES;
    if (textViewEdit.text != self.quote) {
        textWasChanged = YES;
    }else{
        textWasChanged = NO;
    }
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    UITextView *tv = object;
    CGFloat topCorrect = ([tv bounds].size.height - [tv contentSize].height * [tv zoomScale])/2.0;
    topCorrect = ( topCorrect < 0.0 ? 0.0 : topCorrect );
    tv.contentOffset = (CGPoint){.x = 0, .y = -topCorrect};
}

#pragma mark - Action New Font
- (IBAction)fontSelectAction:(id)sender {
    UIButton *btn = (UIButton*)sender;
    for (int i = 1; i <= 6; i++) {
        if (btn.tag == i) {
            btn.selected = YES;
            [btn setTitle:@"A" forState:UIControlStateSelected];
            btn.titleLabel.font = [UIFont fontWithName:[arrayFonts objectAtIndex:(i-1)] size:((IS_IPAD)?34:30)];
            btn.titleLabel.textAlignment = NSTextAlignmentCenter;
            selectedFont = [arrayFonts objectAtIndex:(i-1)];
            textViewEdit.font = [UIFont fontWithName:selectedFont size:((IS_IPAD)?24:18)];
            btn.layer.cornerRadius = 5;
        }else{
            UIButton *btnOther = (UIButton*)[self.view viewWithTag:i];
            btnOther.selected = NO;
            [btnOther setTitle:@"abc" forState:UIControlStateNormal];
            btnOther.titleLabel.font = [UIFont fontWithName:[arrayFonts objectAtIndex:(i-1)] size:((IS_IPAD)?20:12)];
            btnOther.titleLabel.textAlignment = NSTextAlignmentCenter;
            btnOther.layer.cornerRadius = 5;
        }
    }
    if (btn.tag == 0) {
        btn = (UIButton*)[self.view viewWithTag:1];
        btn.selected = YES;
        [btn setTitle:@"A" forState:UIControlStateSelected];
        btn.titleLabel.font = [UIFont fontWithName:[arrayFonts objectAtIndex:0] size:((IS_IPAD)?34:30)];
        btn.titleLabel.textAlignment = NSTextAlignmentCenter;
        selectedFont = [arrayFonts objectAtIndex:0];
        textViewEdit.font = [UIFont fontWithName:selectedFont size:((IS_IPAD)?24:18)];
        btn.layer.cornerRadius = 5;
    }
    if (!isIOS7orHigher()) {
          [self textToAlighnVerticalyCenter];
    }
}

#pragma mark - Text To Center
- (void)textToAlighnVerticalyCenter {
    CGFloat offSet = ([textViewEdit bounds].size.height - [textViewEdit contentSize].height * [textViewEdit zoomScale])/2.0;
    offSet = offSet < 0.0 ? 0.0 : offSet;
    [textViewEdit setContentOffset:CGPointMake(0, -offSet) animated:NO];
}

- (void)centerText
{
    NSTextContainer *container = textViewEdit.textContainer;
    NSLayoutManager *layoutManager = container.layoutManager;
    
    CGRect textRect = [layoutManager usedRectForTextContainer:container];
    UIEdgeInsets inset = UIEdgeInsetsZero;
    inset.top = textViewEdit.bounds.size.height / 2 - textRect.size.height / 2;
    inset.left = textViewEdit.bounds.size.width / 2 - textRect.size.width / 2;
    textViewEdit.textContainerInset = inset;
}

@end
