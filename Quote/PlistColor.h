//
//  PlistColor.h
//  Quote
//
//  Created by Timotin Vanea on 2/6/14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlistColor : NSObject{
    NSDictionary *dictFromPlist;
}
+ (id)sharedColor;
- (NSDictionary*)getDictWithColorsForID:(int8_t)colorId;
@end
