//
//  ReadViewController.h
//  Quote
//
//  Created by Admin on 03.02.14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ReadViewControllerDelegate;

@interface ReadViewController : UIViewController <UITextViewDelegate>{

    __weak IBOutlet UIView *viewWithButtons;
    __weak IBOutlet UIView *viewWithText;
    __weak IBOutlet UIImageView *backgroundImage;
    __weak IBOutlet UITextView *textViewSelectedQuote;
    __weak IBOutlet UILabel *labelWithNameCite;
    IBOutlet UIButton *simpleShare;
    IBOutlet UIButton *redactButton;
    
    IBOutlet UIView *viewWithShareButtons;
    UIImage *imageToShareScreeenShot;
}
- (IBAction)readThisQuote:(id)sender;
- (IBAction)actionSimpleSharing:(id)sender;
- (IBAction)actAdvancedSharing:(id)sender;
- (IBAction)likeThisQuote:(id)sender;
- (IBAction)shareTo:(id)sender;
@property (readwrite, retain) NSString *quote;
@property (readwrite, retain) NSString *cite;
@property (readwrite, retain) NSString *idQuote;
@property (readwrite, retain) NSString *liked;
@property (readwrite, retain) NSString *interpretation;

@property (nonatomic, weak) id<ReadViewControllerDelegate> delegate;
@end

@protocol ReadViewControllerDelegate <NSObject>

- (void)wasLikeInReadView:(BOOL)liked;



@end