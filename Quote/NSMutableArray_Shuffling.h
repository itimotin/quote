//
//  NSMutableArray_Shuffling.h
//  Quote
//
//  Created by Timotin Vanea on 1/6/14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#else
#include <Cocoa/Cocoa.h>
#endif

// This category enhances NSMutableArray by providing
// methods to randomly shuffle the elements.
@interface NSMutableArray (Shuffling)
- (void)shuffle;
@end
