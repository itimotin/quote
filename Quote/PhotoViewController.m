//
//  PhotoViewController.m
//  Quote
//
//  Created by Timotin Vanea on 1/9/14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

#import "PhotoViewController.h"
#import "UIImage+ImageEffects.h"
#import <QuartzCore/QuartzCore.h>
#import "PreviewViewController.h"
#import "ImageScaleProportionaly.h"

@interface PhotoViewController (){
    UIPopoverController *popover;
    UIBarButtonItem *previewButton;
    CGPoint pointCenter;
    float sizeFont;
    CGSize selectedSize;
}

@end

@implementation PhotoViewController
@synthesize editeCite = _editeCite;
@synthesize editedQuote = _editedQuote;
@synthesize strFont = _strFont;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self addButtonToNavigationBar];
    
    if (IS_IPAD) {
        sizeFont = 38;
    }else{
        sizeFont = 30;
    }
    
    self.navigationController.navigationBarHidden = NO;
    
    widthPhoto = WIDTH_DEVICE*2;
    heightPhoto = HEIGHT_DEVICE*2;
    isPortrait = 0;
    
    pointCenter = CGPointMake(self.viewForAllContent.center.x, self.viewForAllContent.center.y);
    
    viewResizeImage.frame = CGRectMake(0, 0, 300, 280);
    viewResizeImage.center = CGPointMake(self.viewForAllContent.frame.size.height/2, self.viewForAllContent.frame.size.width/2);

//    self.backgroundPhoto.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"images.png"]];
    
    [self arangementOnView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    buttonAddPhoto.layer.cornerRadius = 5;
    [self colorAll];
//    [self resizeToWidth:heightPhoto toHeight:widthPhoto];
}

- (void)colorAll
{
    NSDictionary *colorDict = [dictColor getDictWithColorsForID:[[usrDef objectForKey:@"color"] integerValue]];
    self.view.backgroundColor = viewPhotoButtons.backgroundColor = UIColorFromRGB([[colorDict objectForKey:BGColor] integerValue]);
    
    if (!isIOS7orHigher()) {
        [[self navigationController] navigationBar].tintColor = UIColorFromRGB([[colorDict objectForKey:IMG_BGColor] integerValue]);
    }
    sliderForSize.minimumTrackTintColor =  segmentControlPreview.tintColor = buttonAddPhoto.backgroundColor = UIColorFromRGB([[colorDict objectForKey:IMG_BGColor] integerValue]);
    sliderForSize.maximumTrackTintColor = UIColorFromRGB([[colorDict objectForKey:TextColor] integerValue]);
    [buttonAddPhoto setTitleColor:UIColorFromRGB([[colorDict objectForKey:TextColor] integerValue]) forState:UIControlStateNormal];
    
    for (NSInteger i = 1; i <= 3; i++) {
        UIButton *btn = (UIButton*)[viewPhotoButtons viewWithTag:i];
        btn.layer.cornerRadius = 5;
        [btn setTitleColor:UIColorFromRGB([[colorDict objectForKey:TextColor] integerValue]) forState:UIControlStateSelected];
        [btn setTitleColor:UIColorFromRGB([[colorDict objectForKey:TextColor] integerValue]) forState:UIControlStateNormal];
        btn.backgroundColor = UIColorFromRGB([[colorDict objectForKey:IMG_BGColor] integerValue]);
    }
    
}

- (void)arangementOnView{
    if (isIOS7orHigher()) {
        
        if (IS_IPAD) {
            
        }else if (isFourInch){
            
        }else{
            
        }
        
    }else{
        
        if (IS_IPAD) {
            
        }else if (isFourInch){
            
        }else{
            view1.frame = CGRectMake(0, 0, 320, 568);
            view1.center = CGPointMake(WIDTH_DEVICE/2, HEIGHT_DEVICE/2-40);
        }
        segmentControlPreview.segmentedControlStyle = UISegmentedControlStyleBar;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addButtonToNavigationBar{
    previewButton = [[UIBarButtonItem alloc] initWithTitle:@"Preview" style:UIBarButtonItemStylePlain target:self action:@selector(goToPreviewController:)];
    previewButton.enabled = NO;
    self.navigationItem.rightBarButtonItem = previewButton;
    self.title = @"Image";
}

#pragma mark - WORK WITH PICKER
- (void)pickerCamera {
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.allowsEditing = YES;
    picker.delegate = self;
	picker.sourceType = UIImagePickerControllerSourceTypeCamera;
	[self presentViewController:picker animated:YES completion:nil];
}

- (void) pickerLibrary {
	UIImagePickerController * picker = [[UIImagePickerController alloc] init] ;
    picker.allowsEditing = YES;
	picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        popover = [[UIPopoverController alloc] initWithContentViewController:picker];
        popover.delegate = self;
        [popover presentPopoverFromRect:CGRectMake(0.0, 0.0, 200.0, 800.0)
                                 inView:self.view
               permittedArrowDirections:UIPopoverArrowDirectionAny
                               animated:YES];
    }
    else{
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if (IS_IPAD) {
        [popover dismissPopoverAnimated:YES];
        popover = nil;
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
        imageToShare.image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
//    imageToShare.image = [info objectForKey:@"UIImagePickerControllerCropRect"];
        [imageToShare setContentMode:UIViewContentModeScaleAspectFit];

    self.backgroundPhoto.image = imageToShare.image;
    self.backgroundPhotoWithImageEffects.image = [self.backgroundPhoto.image applyLightEffect];
    
    previewButton.enabled = YES;
    [self sliderUpInside:nil];
}

#pragma mark - ACTIONS
- (IBAction)addPhotoAction:(id)sender {
    UIButton *button = (UIButton*)sender;
    switch (button.tag) {
        case 1:
            [self hiddeButtons];
            [self performSelector:@selector(pickerCamera) withObject:nil afterDelay:0.05];
            break;
        case 2:
            [self hiddeButtons];
            [self performSelector:@selector(pickerLibrary) withObject:nil afterDelay:0.05];
            break;
        case 3:
            [self performSelector:@selector(hiddeButtons) withObject:nil afterDelay:0.05];
            break;
        default:
            break;
    }
}


- (IBAction)setOrientationAction:(id)sender {
    UISegmentedControl *segment = (UISegmentedControl*)sender;
    isPortrait = segment.selectedSegmentIndex;
    [self sliderUpInside:nil];
}

- (IBAction)goToPreviewController:(id)sender {
    PreviewViewController* previewController = [self.storyboard instantiateViewControllerWithIdentifier:@"previewId"];
    previewController.editeCite = self.editeCite;
    previewController.editedQuote = self.editedQuote;
    previewController.strFont = self.strFont;
    previewController.fontSize = sizeFont;
    previewController.image = imageToShare.image;
    previewController.imageEffect = self.backgroundPhotoWithImageEffects.image;
    previewController.selectedSize = selectedSize;
    previewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:previewController animated:YES];
}




- (IBAction)addViewChoosePathToPhoto:(id)sender {
    [self showButtons];
    
}

- (IBAction)sliderTuchDown:(id)sender {
    sliderTerminateAction = NO;
    [self showLabel];
}

- (IBAction)sliderUpInside:(id)sender {
    [self hiddenLabel];
    sliderTerminateAction = YES;
    
    if (isPortrait) {
        [self resizeToWidth:widthPhoto toHeight:heightPhoto];
    }else{
        [self resizeToWidth:heightPhoto toHeight:widthPhoto];
    }
}

-(IBAction)valueChangedForSize:(UISlider*)sender {
    int discreteValue = roundl([sender value]); // Rounds float to an integer
    [sender setValue:(float)discreteValue]; // Sets your slider to this value
    switch (discreteValue) {
        case 0:
            heightPhoto = 960.0;
            widthPhoto = 640.0;
            sizeFont = 30;
            break;
        case 1:
            heightPhoto = 1368.0;
            widthPhoto = 640.0;
            sizeFont = 30;
            break;
        case 2:
            heightPhoto = 2048.0;
            widthPhoto = 1536.0;
            sizeFont = 38;
            break;
        case 3:
            heightPhoto = 1368.0;
            widthPhoto = 768.0;
            sizeFont = 30;
            break;
        case 4:
            heightPhoto = 1920.0;
            widthPhoto = 1080.0;
            sizeFont = 35;
            break;
        case 5:
            heightPhoto = 2560.0;
            widthPhoto = 1440.0;
            sizeFont = 40;
            break;
        case 6:
            heightPhoto = 2560.0;
            widthPhoto = 1600.0;
            sizeFont = 42;
            break;
        case 7:
            heightPhoto = 2560.0;
            widthPhoto = 1800.0;
            sizeFont = 48;
            break;
        default:
            break;
    }
    labelSize.text = [NSString stringWithFormat:@"%ld x %ld", (long)heightPhoto,(long)widthPhoto];
}


#pragma mark - FUNCTIONS
- (void)hiddeButtons{
    [UIView animateWithDuration:.5 animations:^{
        viewPhotoButtons.center = CGPointMake(WIDTH_DEVICE/2, HEIGHT_DEVICE+viewPhotoButtons.frame.size.height/2);
        imageShadow.alpha = 0.0;
    } completion:^(BOOL finished){
         if(finished)
        imageShadow.hidden = YES;
    }];
}

-(void)showButtons{
    imageShadow.hidden = NO;
    imageShadow.alpha = 0.0;
    [UIView animateWithDuration:.4 animations:^{
        viewPhotoButtons.center = CGPointMake(WIDTH_DEVICE/2, HEIGHT_DEVICE-viewPhotoButtons.frame.size.height+viewPhotoButtons.frame.size.height/4);
       
        imageShadow.alpha = 1.0;
    } completion:^(BOOL finished){
        }];
}


- (void)hiddenLabel{
    [UIView animateWithDuration:.6 animations:^{
        viewForLabelHidden.alpha = 0.0;
    } completion:^(BOOL finished){
        if(finished)
            viewForLabelHidden.hidden = YES;
    }];
}

-(void)showLabel{
    viewForLabelHidden.hidden = NO;
    viewForLabelHidden.alpha = 0.0;
    [UIView animateWithDuration:.4 animations:^{
        viewForLabelHidden.alpha = 0.6;
    } completion:^(BOOL finished){
    }];
}

#pragma mark IMAGES
- (void)resizeToWidth:(double)width toHeight:(double)height{
    selectedSize = CGSizeMake(width, height);
     self.viewForAllContent.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);

    
    self.viewForAllContent.frame = CGRectMake(0.0f, 0.0f, width, height);
    self.backgroundPhoto.contentMode = self.backgroundPhotoWithImageEffects.contentMode = UIViewContentModeScaleAspectFill;
    
    if(isPortrait){
        if (imageToShare.image.size.width< imageToShare.image.size.height) {
            if (imageToShare.image.size.height< height) {
                viewResizeImage.frame = CGRectMake(0, 0, imageToShare.image.size.width, imageToShare.image.size.height);
              //NSLog(@"44");
            }else{
                viewResizeImage.frame = CGRectMake(0, 0, width, height);
                //NSLog(@"4");
            }
        }else{
            if (imageToShare.image.size.width< width) {
               viewResizeImage.frame = CGRectMake(0, 0, imageToShare.image.size.width, imageToShare.image.size.height);
              //NSLog(@"33");
            }else{
                viewResizeImage.frame = CGRectMake(0, 0, width, height);
                //NSLog(@"3");
            }
        }
        //============================================================================
    }else{
    
        if (imageToShare.image.size.width< imageToShare.image.size.height) {
            if (imageToShare.image.size.height< height) {
                viewResizeImage.frame = CGRectMake(0, 0, imageToShare.image.size.width, imageToShare.image.size.height);
            }else{
                viewResizeImage.frame = CGRectMake(0, 0, width, height);
            }
        }else{
            if (imageToShare.image.size.width< width) {
                viewResizeImage.frame = CGRectMake(0, 0, imageToShare.image.size.width, imageToShare.image.size.height);
            }else{
                viewResizeImage.frame = CGRectMake(0, 0, width, height);
            }
        }
        

        //============================================================================
    }
    
    viewResizeImage.center = CGPointMake(self.viewForAllContent.frame.size.width/2.0, self.viewForAllContent.frame.size.height/2.0);
    imageToShare.contentMode = UIViewContentModeScaleAspectFit;
    
    double newScale = 0.0;
    if (isPortrait) {
        newScale = ((IS_IPAD)?560:280)/height;
    }else{
        newScale = ((IS_IPAD)?560:280)/width;
    }
    
    self.backgroundPhoto.center = self.backgroundPhotoWithImageEffects.center = CGPointMake(self.viewForAllContent.frame.size.width/2.0, self.viewForAllContent.frame.size.height/2.0);

    self.viewForAllContent.transform = CGAffineTransformScale(CGAffineTransformIdentity, newScale, newScale);
    self.viewForAllContent.center = pointCenter;
}


@end
