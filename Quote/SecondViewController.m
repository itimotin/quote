//
//  SecondViewController.m
//  Quote
//
//  Created by Timotin Vanea on 12/22/13.
//  Copyright (c) 2013 Timotin Vanea. All rights reserved.
//

#import "SecondViewController.h"
#import "StorageResource.h"

@interface SecondViewController (){
    NSMutableArray *arrayWithQuotes;
    NSMutableArray *arrMicForUse;
    NSInteger rowSelected;
}

@end

@implementation SecondViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self arangementOnView];
    
    arrayWithQuotes = [[NSMutableArray alloc] init];
    arrayWithQuotes = [storageDB getAllQuotes];
    
    arrMicForUse = [NSMutableArray array];
    [self addItemsForArray];
    
    tableViewQuote.layer.cornerRadius = ((IS_IPAD)?20:10);
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [self colorAll];
}

- (void)colorAll
{
    NSDictionary *colorDict = [dictColor getDictWithColorsForID:[[usrDef objectForKey:@"color"] integerValue]];
    self.view.backgroundColor = UIColorFromRGB([[colorDict objectForKey:BGColor] integerValue]);
    segmentBarController.tintColor = UIColorFromRGB([[colorDict objectForKey:IMG_BGColor] integerValue]);
    if (isIOS7orHigher()) {
        searchBarQuote.barTintColor = UIColorFromRGB([[colorDict objectForKey:IMG_BGColor] integerValue]);
        [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:                                           UIColorFromRGB([[colorDict objectForKey:TextColor] integerValue]), UITextAttributeTextColor,[UIColor clearColor],                      UITextAttributeTextShadowColor, [NSValue valueWithUIOffset:UIOffsetMake(0, -1)], UITextAttributeTextShadowOffset, nil] forState:UIControlStateNormal];
    }else{
        [[self navigationController] navigationBar].tintColor = UIColorFromRGB([[colorDict objectForKey:IMG_BGColor] integerValue]);
        searchBarQuote.tintColor = UIColorFromRGB([[colorDict objectForKey:IMG_BGColor] integerValue]);
        segmentBarController.segmentedControlStyle = UISegmentedControlStyleBar;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)arangementOnView {
    if (isIOS7orHigher()) {
        [self.tabBarController.tabBar setTranslucent:NO];
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
        if (IS_IPAD) {
            
        }else if (isFourInch){
            
        }else{
            
        }
        
    }else{
        
        if (IS_IPAD) {
            searchBarQuote.center = CGPointMake(searchBarQuote.frame.size.width/2, searchBarQuote.frame.size.height/2);
            segmentBarController.center = CGPointMake(WIDTH_DEVICE/2, searchBarQuote.center.y+searchBarQuote.frame.size.height/2+segmentBarController.frame.size.height/2+10);
            tableViewQuote.frame = CGRectMake(20.0, segmentBarController.center.y+ segmentBarController.frame.size.height, tableViewQuote.frame.size.width, tableViewQuote.frame.size.height+40);
        }else if (isFourInch){
            searchBarQuote.center = CGPointMake(searchBarQuote.frame.size.width/2, searchBarQuote.frame.size.height/2);
            
            segmentBarController.center = CGPointMake(WIDTH_DEVICE/2, searchBarQuote.center.y+searchBarQuote.frame.size.height/2+segmentBarController.frame.size.height/2+5);
            
            tableViewQuote.frame = CGRectMake(10.0, segmentBarController.center.y+ segmentBarController.frame.size.height+5, tableViewQuote.frame.size.width, tableViewQuote.frame.size.height);
        }else{
            searchBarQuote.center = CGPointMake(searchBarQuote.frame.size.width/2, searchBarQuote.frame.size.height/2);
            
            segmentBarController.center = CGPointMake(WIDTH_DEVICE/2, searchBarQuote.center.y+searchBarQuote.frame.size.height/2+segmentBarController.frame.size.height/2+5);
            
            tableViewQuote.frame = CGRectMake(10.0, segmentBarController.center.y+ segmentBarController.frame.size.height+5, tableViewQuote.frame.size.width, tableViewQuote.frame.size.height);
        }
    }
}

#pragma mark - Text To Center
- (void)alighnVerticalyThisText:(UITextView*)txVi {
    CGFloat offSet = ([txVi bounds].size.height - [txVi contentSize].height * [txVi zoomScale])/2.0;
    offSet = offSet < 0.0 ? 0.0 : offSet;
    [txVi setContentOffset:CGPointMake(0, -offSet) animated:NO];
    [txVi setTextAlignment:NSTextAlignmentCenter];
}


#pragma mark - TableViewController
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrMicForUse.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"cellId1";
    NSDictionary *dict = [arrMicForUse objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    UITextView *textViQuote = (UITextView*)[cell viewWithTag:TAG_TEXT];
    textViQuote.text = [dict objectForKey:@"quote"];
    [textViQuote setFont:[UIFont fontWithName:FONT_DefoRegular size:((IS_IPAD)?19:13)]];
    
    [self alighnVerticalyThisText:textViQuote];
    
    UILabel *lbl = (UILabel*)[cell viewWithTag:TAG_LABEL];
    lbl.text = [dict objectForKey:@"cite"];
    
    UIButton *btnLike = (UIButton*)[cell viewWithTag:TAG_BUTTON];
    btnLike.selected = [[dict objectForKey:@"liked"] integerValue];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger i = 0;
    i = arrayWithQuotes.count;
    if (indexPath.row+1 == arrMicForUse.count && arrMicForUse.count != i) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                                 (unsigned long)NULL), ^(void) {
            [self addItemsForArray];
            dispatch_sync(dispatch_get_main_queue(), ^{
                [tableView reloadData];
            });
        });
    }else if(arrMicForUse.count == arrayWithQuotes.count){
    }
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    self.view.userInteractionEnabled = NO;
    rowSelected = indexPath.row;
    NSDictionary *dict = [arrMicForUse objectAtIndex:indexPath.row];
    ReadViewController *readVi = [self.storyboard instantiateViewControllerWithIdentifier:@"ReadViId"];
    readVi.delegate = self;
    
    readVi.quote = [dict objectForKey:@"quote"];
    readVi.cite = [dict objectForKey:@"cite"];
    readVi.idQuote = [dict objectForKey:@"id"];
    readVi.liked = [dict objectForKey:@"liked"];
    readVi.interpretation = [dict objectForKey:@"interpretation"];
    
    [self.navigationController pushViewController:readVi animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.view.userInteractionEnabled = YES;
}
#pragma mark - Add Items
- (void)addItemsForArray{
    NSUInteger n = arrMicForUse.count + NR_ITEM_TO_ADD;
    if (n > arrayWithQuotes.count) {
        n = arrayWithQuotes.count;
    }
    for (NSUInteger i = arrMicForUse.count; i<n; i++) {
        [arrMicForUse addObject:[arrayWithQuotes objectAtIndex:i]];
    }
}

#pragma mark - LIKE BUTTON
- (IBAction)likeQuoteAction:(id)sender event:(id)event {
    
    UIButton *btn = (UIButton*)sender;
    btn.selected = !btn.selected;
    NSString* select = [NSString stringWithFormat:@"%d",btn.selected];
    
    CGPoint touchPosition = [[[event allTouches] anyObject] locationInView:tableViewQuote];
    NSIndexPath *indexPath = [tableViewQuote indexPathForRowAtPoint:touchPosition];
    
    if (indexPath != nil)
    {
        BOOL b;
        NSDictionary *dict = [arrayWithQuotes objectAtIndex:indexPath.row];
        NSString *idQuote = [dict objectForKey:@"id"];
        NSDictionary *newDict = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:idQuote, [dict objectForKey:@"category"],[dict objectForKey:@"cite"],[dict objectForKey:@"interpretation"],[dict objectForKey:@"readed"],[dict objectForKey:@"quote"], select, nil] forKeys:[NSArray arrayWithObjects:@"id",@"category",@"cite", @"interpretation", @"readed", @"quote", @"liked", nil]];
        
        if (btn.selected) {
            [arrayWithQuotes replaceObjectAtIndex:indexPath.row withObject:newDict];
            [arrMicForUse replaceObjectAtIndex:indexPath.row withObject:newDict];
            b = [storageDB addToFavoritesQuoteWithId:idQuote];
        }else{
            [arrayWithQuotes replaceObjectAtIndex:indexPath.row withObject:newDict];
            [arrMicForUse replaceObjectAtIndex:indexPath.row withObject:newDict];
            b = [storageDB removeFromFavoritesQuoteWithId:idQuote];
        }
        
        [tableViewQuote reloadData];
        NSLog(@"SCRIEREA BOOL (%d)",b);
    }
}

- (void)wasLikeInReadView:(BOOL)liked{
    NSDictionary *dict = [arrayWithQuotes objectAtIndex:rowSelected];
    NSString *idQuote = [dict objectForKey:@"id"];
    NSDictionary *newDict = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:idQuote, [dict objectForKey:@"category"],[dict objectForKey:@"cite"],[dict objectForKey:@"interpretation"],[dict objectForKey:@"readed"],[dict objectForKey:@"quote"], @(liked), nil] forKeys:[NSArray arrayWithObjects:@"id",@"category",@"cite", @"interpretation", @"readed", @"quote", @"liked", nil]];
    [arrayWithQuotes replaceObjectAtIndex:rowSelected withObject:newDict];
    [arrMicForUse replaceObjectAtIndex:rowSelected withObject:newDict];
    [tableViewQuote reloadData];
}

#pragma mark - Search
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    searchBar.showsCancelButton = YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    [self dismissKeyboard];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    NSMutableArray * mutArr = [storageDB getAllForSearchedText:searchBar.text];
    
    if (mutArr.count) {
        [arrayWithQuotes removeAllObjects];
        [arrMicForUse removeAllObjects];
        arrayWithQuotes = mutArr;
        [self addItemsForArray];
        [tableViewQuote reloadData];
    }
    [self dismissKeyboard];
}

- (void) dismissKeyboard
{
    // add self
    [searchBarQuote resignFirstResponder];
    searchBarQuote.showsCancelButton = NO;
    [searchBarQuote setText:nil];
    
}

#pragma mark - Segment
- (IBAction)segmentAction:(id)sender {
    UISegmentedControl *segment = (UISegmentedControl*)sender;
    NSMutableArray *arr;
    switch (segment.selectedSegmentIndex) {
        case 0:
            arr = [storageDB getAllQuotes];
            break;
        case 1:
            arr = [storageDB getAllForType:[NSNumber numberWithInteger:3]];
            break;
        case 2:
            arr = [storageDB getAllForType:[NSNumber numberWithInteger:segment.selectedSegmentIndex]];
            break;
        case 3:
            arr = [storageDB getAllFavorites];
            break;
        default:
            break;
    }
    if (arr.count) {
        [arrayWithQuotes removeAllObjects];
        [arrMicForUse removeAllObjects];
        arrayWithQuotes = arr;
        [self addItemsForArray];
        [tableViewQuote reloadData];
        [self tableViewScroolToTop];
    }else{
        
    }
    
}

- (void)tableViewScroolToTop{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [tableViewQuote scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

@end
