//
//  FirstViewController.m
//  Quote
//
//  Created by Timotin Vanea on 12/22/13.
//  Copyright (c) 2013 Timotin Vanea. All rights reserved.
//

#import "FirstViewController.h"
#import "StorageResource.h"
#import "NSMutableArray_Shuffling.h"
#import "EditQuoteViewController.h"
#import "PreviewViewController.h"

#import "SHKFacebook.h"
#import "SHKVkontakte.h"
#import "SHKTwitter.h"
#import "SHKMail.h"
#import "SHKDropbox.h"
#import "SHKGooglePlus.h"
#import "SHKFlickr.h"
#import "SHKTumblr.h"
#import "SHK.h"
#import "SHKItem.h"
#import "SHKSharer.h"

#import <AssetsLibrary/AssetsLibrary.h>

@interface FirstViewController (){
    NSDictionary *dictQuoteOfDay;
}
-(NSString *)addingQoutesForText:(NSString*)myText;
@end

@implementation FirstViewController
@synthesize quoteTextView;

// TODO: ADD LIKE BUTTON

- (void)viewDidLoad
{
    if (!isIOS7orHigher()) {
        [quoteTextView addObserver:self forKeyPath:@"contentSize" options:(NSKeyValueObservingOptionNew) context:NULL];
    }
    
    [super viewDidLoad];
    [self prepareForUse];
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    dictQuoteOfDay = [self giveDailyQuote];
    
    quoteTextView.text = [dictQuoteOfDay objectForKey:@"quote"];
    name.text = [dictQuoteOfDay objectForKey:@"cite"];
    [self arangementOnView];
    [self addNotification];
}

- (void)arangementOnView{
    
    if (isIOS7orHigher()) {
        if (IS_IPAD) {
            
        }else if (isFourInch){
            
        }else{
            
        }
        
    }else{
        
        if (IS_IPAD) {
            
        }else if (isFourInch){
            workingView.center = CGPointMake(WIDTH_DEVICE/2, WIDTH_DEVICE/2);
            viewWithButtons.center = CGPointMake(WIDTH_DEVICE/2, HEIGHT_DEVICE/2+100);
        }else{
            
            workingView.center = CGPointMake(WIDTH_DEVICE/2, WIDTH_DEVICE/2);
            viewWithButtons.center = CGPointMake(WIDTH_DEVICE/2, 360.0);
            
        }
        [self.quoteTextView removeObserver:self forKeyPath:@"contentSize"];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    quoteTextView.layer.cornerRadius = redactButton.layer.cornerRadius = simpleShare.layer.cornerRadius = 5;
    [self colorAll];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    if (isIOS7orHigher()) {
        [self centerText];
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
    }else{
        [self textToAlighnVerticalyCenter];
    }
}

- (void)colorAll
{
    NSDictionary *colorDict = [dictColor getDictWithColorsForID:[[usrDef objectForKey:@"color"] integerValue]];
    self.view.backgroundColor = UIColorFromRGB([[colorDict objectForKey:BGColor] integerValue]);
    
    redactButton.backgroundColor = simpleShare.backgroundColor = UIColorFromRGB([[colorDict objectForKey:IMG_BGColor] integerValue]);
    [redactButton setTitleColor:UIColorFromRGB([[colorDict objectForKey:TextColor] integerValue]) forState:UIControlStateNormal];
    [simpleShare setTitleColor:UIColorFromRGB([[colorDict objectForKey:TextColor] integerValue]) forState:UIControlStateNormal];
    //    name.textColor = quoteTextView.textColor = workingView.backgroundColor = UIColorFromRGB([[colorDict objectForKey:TextColor] integerValue]);
}

#pragma mark Text To Center
- (void)textToAlighnVerticalyCenter {
    CGFloat offSet = ([self.quoteTextView bounds].size.height - [self.quoteTextView contentSize].height * [self.quoteTextView zoomScale])/2.0;
    offSet = offSet < 0.0 ? 0.0 : offSet;
    [self.quoteTextView setContentOffset:CGPointMake(0, -offSet) animated:NO];
}

- (void)centerText
{
    NSTextContainer *container = self.quoteTextView.textContainer;
    NSLayoutManager *layoutManager = container.layoutManager;
    
    CGRect textRect = [layoutManager usedRectForTextContainer:container];
    
    UIEdgeInsets inset = UIEdgeInsetsZero;
    inset.top = self.quoteTextView.bounds.size.height / 2 - textRect.size.height / 2;
    inset.left = self.quoteTextView.bounds.size.width / 2 - textRect.size.width / 2;
    
    self.quoteTextView.textContainerInset = inset;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
}

- (NSDictionary*)giveDailyQuote{
    NSInteger pastedDays = [self getNumberOfPastedDay];
    NSString *idQuote = [[usrDef objectForKey:SET_ARRAY_KEY] objectAtIndex:pastedDays];
    return [storageDB dictForID:idQuote];
}


- (NSInteger)getNumberOfPastedDay{
    NSDate *startDate = [usrDef objectForKey:SET_FIRST_LAUNCH_APP];
    NSDate *endDate = [NSDate date];
    
    NSTimeInterval distanceBetweenDates = [endDate timeIntervalSinceDate:startDate];
    double secondsInAnHour = 3600*24;
    
    return (NSInteger)(distanceBetweenDates / secondsInAnHour);
}

- (BOOL)uniqueQuoteWithID:(NSInteger)idQuote{
    return YES;
}

-(NSString *)addingQoutesForText:(NSString*)myText{
    NSString *textToReturn = [NSString stringWithFormat:@"\"%@\"",myText];
    return textToReturn;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ACTIONS
- (IBAction)simpleShare:(id)sender {
    viewWithButtons.hidden = YES;
    viewWithShareButtons.hidden = YES;
    
    UIGraphicsBeginImageContextWithOptions(workingView.frame.size, NO, 0.0);
    [workingView.layer renderInContext:UIGraphicsGetCurrentContext()];
    imageToShareScreeenShot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    viewWithShareButtons.hidden = NO;
    viewWithButtons.hidden = NO;
    [self showViewWithButtons];
}

- (IBAction)redactAction:(id)sender {
    EditQuoteViewController* editViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"EditId"];
    editViewController.quote = [dictQuoteOfDay objectForKey:@"quote"];
    editViewController.cite = [dictQuoteOfDay objectForKey:@"cite"];
    [self.navigationController pushViewController:editViewController animated:YES];
}

- (IBAction)shareTo:(id)sender
{
    [self hiddenViewWithButtons];
    NSInteger tag = ((UIButton*)sender).tag;
    SHKItem *item=[[SHKItem alloc]init];
    item.shareType=SHKShareTypeImage;
    item.title=@"Feed your brain";
    item.image = imageToShareScreeenShot;
    switch (tag) {
        case 1:
            //facebook
            [SHKFacebook shareItem:item];
            break;
        case 2:
            //library
            UIImageWriteToSavedPhotosAlbum(imageToShareScreeenShot, nil, nil, nil);
            break;
        case 3:
            //twit
            [SHKTwitter shareItem:item];
            break;
        case 4:
            //tumblr
            [SHKTumblr shareItem:item];
            break;
        case 5:
            //flickr
            [SHKFlickr shareItem:item];
            break;
        case 6:
            //mail
            [SHKMail shareItem:item];
            break;
        case 7:
            //vk
            [SHKVkontakte shareItem:item];
            break;
        case 8:
            //dropbox
            [SHKDropbox shareItem:item];
            break;
        case 9:
            //g+
          
            break;
            
            
        default:
            break;
    }
    

}

-(void)showViewWithButtons {
    viewWithShareButtons.alpha = 1.0f;
    [UIView animateWithDuration:.5 animations:^{
        
        viewWithShareButtons.center = CGPointMake(viewWithShareButtons.frame.size.width/2, HEIGHT_DEVICE/2-((isIOS7orHigher())?0:65));
    } completion:^(BOOL finished){
        
    }];
}

- (void)hiddenViewWithButtons {
//    [shareButton setImage:[UIImage imageNamed:@"share"]];
    [UIView animateWithDuration:.5 animations:^{
        viewWithShareButtons.center = CGPointMake(-viewWithShareButtons.frame.size.width/2, HEIGHT_DEVICE/2-((isIOS7orHigher())?0:65));
    } completion:^(BOOL finished){
        viewWithShareButtons.alpha = 0.0f;
    }];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    UITextView *tv = object;
    CGFloat topCorrect = ([tv bounds].size.height - [tv contentSize].height * [tv zoomScale])/2.0;
    topCorrect = ( topCorrect < 0.0 ? 0.0 : topCorrect );
    tv.contentOffset = (CGPoint){.x = 0, .y = -topCorrect};
}
#pragma mark - ADD NOTIFICATION

- (void)addNotification
{
    NSString *str = [usrDef objectForKey:SET_NOTIFICATION_KEY];
    if (!str) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"About notification"
                                                        message:@"Do you want to receive quotes (notifications) everyday?"
                                                       delegate:self
                                              cancelButtonTitle:@"NO"
                                              otherButtonTitles:@"YES", nil];
        [alert show];
    }
}

#pragma mark - PREPARE FOR USE
- (void)prepareForUse{
    if ([storageDB giveNumberOfQuotes]!=[[usrDef objectForKey:SET_ARRAY_KEY] count]) {
        NSMutableArray *arr = [storageDB giveIndexesOfQuotes];
        [arr shuffle];
        [usrDef setObject:arr forKey:SET_ARRAY_KEY];

        [usrDef setObject:[self getCurrentDayInDateFormat] forKey:SET_FIRST_LAUNCH_APP];
        
        // aici salvam pentru fiecare zi un quote, dar lua prea mult timp din aceasta cauza nu folosesc astfel de metoda.
        //        [storageDB addDaterNotificationForIndex:arr];
    }
}

- (NSDate*)getCurrentDayInDateFormat{
    NSDate *dateToFire = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:dateToFire];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    [components setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    return [calendar dateFromComponents:components];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
}

- (void)yesINeedQuotesNotification
{
    NSDate *dateToFire = [NSDate date];
//    dateToFire = [dateToFire dateByAddingTimeInterval:24*3600];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSTimeZoneCalendarUnit) fromDate:dateToFire];
    NSInteger day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    [components setYear:year];
    [components setMonth:month];
    [components setDay:day+1];
    [components setHour:9];
    [components setMinute:0];
    [components setSecond:0];
    dateToFire = [calendar dateFromComponents:components];
    NSLog(@"DATE FIRE %@ ", dateToFire);
    
    //        NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    //        [dateFormatters setDateFormat:@"yyyy-MM-dd hh:mm:MM"];
    //        [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    //        NSLog(@"DateString : %@", [dateFormatters stringFromDate: dateToFire]);
    
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = dateToFire;
    localNotification.alertBody = @"Tap to view Quote of the day ...";
    localNotification.repeatCalendar = [NSCalendar currentCalendar];
    localNotification.repeatInterval = kCFCalendarUnitDay;
    localNotification.alertAction = NSLocalizedString(@"View Notification Details", nil);
    localNotification.timeZone = [NSTimeZone systemTimeZone];
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
    // Request to reload table view data
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadData" object:self];
}

#pragma mark - Alert Delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        [usrDef setInteger:2 forKey:SET_NOTIFICATION_KEY];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [self yesINeedQuotesNotification];
        [usrDef setInteger:1 forKey:SET_NOTIFICATION_KEY];
    }
}

@end
