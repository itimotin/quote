//
//  PreviewViewController.h
//  Quote
//
//  Created by Timotin Vanea on 1/20/14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreviewViewController : UIViewController{

    IBOutlet UIView *viewWithAll;
    IBOutlet UIImageView *background;
    IBOutlet UIImageView *backgroundEffect;
    IBOutlet UIView *viewCenterImage;
    IBOutlet UIImageView *imageCenter;
    
    IBOutlet UIImageView *imageShadowForText;

    UILabel *labelQuote;
    IBOutlet UIView *viewWithButtonsShare;
}

@property (readwrite, retain) NSString *editedQuote;
@property (readwrite, retain) NSString *editeCite;
@property (readwrite, retain) NSString *strFont;
@property (readwrite) float fontSize;
@property (readwrite, retain) UIImage *image;
@property (readwrite, retain) UIImage *imageEffect;
@property (readwrite) CGSize selectedSize;

- (IBAction)shareTo:(id)sender;

@end
