//
//  ImageScaleProportionaly.h
//  Quote
//
//  Created by Timotin Vanea on 1/24/14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

@interface UIImage (Extras)
- (UIImage *)imageByScalingProportionallyToSize:(CGSize)targetSize;
@end;
