//
//  SettingsViewController.m
//  Quote
//
//  Created by Timotin Vanea on 1/8/14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

#import "SettingsViewController.h"
#import "StorageResource.h"
@interface SettingsViewController (){

}

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    [textField resignFirstResponder];
    return NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    for (int8_t i = 1; i<=10; i++) {
        
        NSDictionary * dict = [dictColor getDictWithColorsForID:i];
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake((i-1)*viewButtons.frame.size.width/10, 0, viewButtons.frame.size.width/10, viewButtons.frame.size.height)];
        btn.layer.cornerRadius = 5;
        btn.tag = i;
        
        btn.backgroundColor = UIColorFromRGB([[dict objectForKey:BGColor] integerValue]);
        [btn addTarget:self action:@selector(setColorForAll:) forControlEvents:UIControlEventTouchUpInside];
        
        [viewButtons addSubview:btn];
    
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    int8_t i = [usrDef integerForKey:@"color"];
    NSDictionary *dict = [dictColor getDictWithColorsForID:i];
    
    buttonRefreshReadedQuotes.backgroundColor = UIColorFromRGB([[dict objectForKey:IMG_BGColor] integerValue]);
    
    lableInstructionName.textColor = labelNotif.textColor = UIColorFromRGB([[dict objectForKey:TextColor] integerValue]);
    
    
    [buttonRefreshReadedQuotes setTitleColor:UIColorFromRGB([[dict objectForKey:TextColor] integerValue]) forState:UIControlStateNormal];
    
    self.view.backgroundColor = UIColorFromRGB([[dict objectForKey:BGColor] integerValue]);
    
    buttonRefreshReadedQuotes.layer.cornerRadius = 5;
    NSInteger selectNotification = [usrDef integerForKey:SET_NOTIFICATION_KEY];
    [self.notificationSwitch setOn:((selectNotification<2)?YES:NO) animated:NO];
    
    if (!isIOS7orHigher() && !IS_IPAD) {
            [self.notificationSwitch setCenter:CGPointMake(WIDTH_DEVICE-50, self.notificationSwitch.center.y)];
    }

     NSString *name =[usrDef objectForKey:Author];
    if (!name.length) {
        uitextFieldWithName.placeholder = @"Type Here Your Name ...";
    }else
        uitextFieldWithName.text = name;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)setColorForAll:(id)sender {
    UIButton *btn = (UIButton*)sender;
    [usrDef setObject:[NSString stringWithFormat:@"%ld", (long)btn.tag] forKey:@"color"];
    NSDictionary *dict = [dictColor getDictWithColorsForID:btn.tag];
    
    buttonRefreshReadedQuotes.backgroundColor = UIColorFromRGB([[dict objectForKey:IMG_BGColor] integerValue]);
    
    [buttonRefreshReadedQuotes setTitleColor:UIColorFromRGB([[dict objectForKey:TextColor] integerValue]) forState:UIControlStateNormal];
   lableInstructionName.textColor = labelNotif.textColor = UIColorFromRGB([[dict objectForKey:TextColor] integerValue]);
    self.view.backgroundColor = UIColorFromRGB([[dict objectForKey:BGColor] integerValue]);
}

- (void)editLabelWhatDayAreSetted {

}


- (IBAction)switchAction:(id)sender {
    UISwitch *switNotif = (UISwitch *)sender;
    if (switNotif.isOn) {
        [usrDef setInteger:1 forKey:SET_NOTIFICATION_KEY];
        [self yesINeedQuotesNotification];
    }else{
        [usrDef setInteger:2 forKey:SET_NOTIFICATION_KEY];
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
    }
}



- (IBAction)refreshQuotes:(id)sender {
    BOOL b = [storageDB refreshAllReaded];
    NSLog(@"BOOL (%d)", b);
}


#pragma mark - Notification

- (void)yesINeedQuotesNotification{
    NSDate *dateToFire = [NSDate date];
    dateToFire = [dateToFire dateByAddingTimeInterval:24*3600];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSTimeZoneCalendarUnit) fromDate:dateToFire];
    NSInteger day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    [components setYear:year];
    [components setMonth:month];
    [components setDay:day+1];
    [components setHour:9];
    [components setMinute:0];
    [components setSecond:0];
    dateToFire = [calendar dateFromComponents:components];
    
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = dateToFire;
    localNotification.alertBody = @"Press to view Quote of the day ...";
    localNotification.repeatCalendar = [NSCalendar currentCalendar];
    localNotification.repeatInterval = kCFCalendarUnitDay;
    localNotification.alertAction = NSLocalizedString(@"View Notification Details", nil);
    localNotification.timeZone = [NSTimeZone systemTimeZone];
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
    // Request to reload table view data
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadData" object:self];
}


@end
