//
//  SecondViewController.h
//  Quote
//
//  Created by Timotin Vanea on 12/22/13.
//  Copyright (c) 2013 Timotin Vanea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReadViewController.h"

@interface SecondViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UITabBarControllerDelegate, UITabBarDelegate,ReadViewControllerDelegate> {

    IBOutlet UISearchBar *searchBarQuote;
    __weak IBOutlet UITableView *tableViewQuote;
    IBOutlet UISegmentedControl *segmentBarController;
//    NSMutableArray *searchedArray;
//    BOOL isSearch, isAll, isLove;
}
- (IBAction)likeQuoteAction:(id)sender event:(id)event;
- (IBAction)segmentAction:(id)sender;


@end
