//
//  AddQuoteViewController.m
//  Quote
//
//  Created by Timotin Vanea on 2/14/14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

#import "AddQuoteViewController.h"
#import "ReadViewController.h"

@interface AddQuoteViewController (){
    NSMutableArray *arrayWithQuotes, *arrMicForUse;
    BOOL openForEdit;
    NSNumber* idQuoteSelectForEdit;
}

@end

@implementation AddQuoteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self arangementOnView];
    openForEdit = NO;
    arrayWithQuotes = [[NSMutableArray alloc] init];
    arrayWithQuotes = [storageDB getMyQuotes];
    
    arrMicForUse = [NSMutableArray array];
    [self addItemsForArray];
    self.tableWithMyQuotes.layer.cornerRadius = ((IS_IPAD)?20:10);
//    self.tableWithMyQuotes.editing = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self colorAll];
    [self controlIfIsSettedName];
}

- (void)arangementOnView{
    if (isIOS7orHigher()) {
        [self.tabBarController.tabBar setTranslucent:NO];
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
        
        if (IS_IPAD) {
            
        }else if (isFourInch){
            
        }else{
            self.tableWithMyQuotes.frame = CGRectMake(10.0, viewWithText.center.y+viewWithText.frame.size.height/2+40, self.tableWithMyQuotes.frame.size.width, 170.0);
        }
        
    }else{
        
        if (IS_IPAD) {
            
        }else if (isFourInch){
            self.tableWithMyQuotes.frame = CGRectMake(10.0, viewWithText.center.y+ viewWithText.frame.size.height/2+45, self.tableWithMyQuotes.frame.size.width, 200.0);
        }else{
            
            self.tableWithMyQuotes.frame = CGRectMake(10.0, viewWithText.center.y+ viewWithText.frame.size.height/2+40, self.tableWithMyQuotes.frame.size.width, 170.0);
        }
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SET NAME
- (void)controlIfIsSettedName{
    NSString *name =[usrDef objectForKey:Author];
    if (!name.length) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Author quote"
                                                        message:@"Please insert your name or nickname:"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"OK", nil];
        [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
        [alert show];
    }
}

- (void)colorAll
{
    NSDictionary *colorDict = [dictColor getDictWithColorsForID:[[usrDef objectForKey:@"color"] integerValue]];
    self.view.backgroundColor = UIColorFromRGB([[colorDict objectForKey:BGColor] integerValue]);
    if (isIOS7orHigher()) {
        textViewWithMyQuote.tintColor = UIColorFromRGB([[colorDict objectForKey:TextColor] integerValue]);
    }
    textViewWithMyQuote.textColor = viewWithText.backgroundColor = UIColorFromRGB([[colorDict objectForKey:TextColor] integerValue]);
    textViewWithMyQuote.backgroundColor = UIColorFromRGB([[colorDict objectForKey:IMG_BGColor] integerValue]);

}


#pragma mark - TableViewController
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrMicForUse.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"MyQuoteCell_id";
    NSDictionary *dict = [arrMicForUse objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    UITextView *textViQuote = (UITextView*)[cell viewWithTag:TAG_TEXT];
    textViQuote.text = [dict objectForKey:@"quote"];
    [textViQuote setFont:[UIFont fontWithName:FONT_DefoRegular size:((IS_IPAD)?19:13)]];
    if (isIOS7orHigher()) {
        // lucreaza doar daca nu este activat scrooling.
        //        [self centerThisText:textViQuote];
        [textViQuote setTextAlignment:NSTextAlignmentCenter];
    }else
    {
        [self alighnVerticalyThisText:textViQuote];
    }
    
    UILabel *lbl = (UILabel*)[cell viewWithTag:TAG_LABEL];
    lbl.text = [dict objectForKey:@"cite"];
    
    UIButton *btnLike = (UIButton*)[cell viewWithTag:TAG_BUTTON];
    btnLike.selected = [[dict objectForKey:@"liked"] integerValue];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger i = 0;
    i = arrayWithQuotes.count;
    if (indexPath.row+1 == arrMicForUse.count && arrMicForUse.count != i) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                                 (unsigned long)NULL), ^(void) {
            [self addItemsForArray];
            dispatch_sync(dispatch_get_main_queue(), ^{
                [tableView reloadData];
            });
        });
    }else if(arrMicForUse.count == arrayWithQuotes.count){
    }
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.view.userInteractionEnabled = NO;
    idQuoteSelectForEdit = [NSNumber numberWithInteger:indexPath.row];
    
    NSDictionary *dict = [arrMicForUse objectAtIndex:indexPath.row];

    ReadViewController *readVi = [self.storyboard instantiateViewControllerWithIdentifier:@"ReadViId"];
    readVi.quote = [dict objectForKey:@"quote"];
    readVi.cite = [dict objectForKey:@"cite"];
    readVi.idQuote = [dict objectForKey:@"id"];
    readVi.liked = [NSString stringWithFormat:@"100"];
    
    [self.navigationController pushViewController:readVi animated:YES];

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.view.userInteractionEnabled = YES;
}
//
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//    return  YES;
//}

#pragma mark - TEXT CENTRED
- (void)alighnVerticalyThisText:(UITextView*)txVi {
    CGFloat offSet = ([txVi bounds].size.height - [txVi contentSize].height * [txVi zoomScale])/2.0;
    offSet = offSet < 0.0 ? 0.0 : offSet;
    [txVi setContentOffset:CGPointMake(0, -offSet) animated:NO];
    [txVi setTextAlignment:NSTextAlignmentCenter];
}

- (void)centerThisText:(UITextView*)txVi
{
    NSTextContainer *container = txVi.textContainer;
    NSLayoutManager *layoutManager = container.layoutManager;
    
    CGRect textRect = [layoutManager usedRectForTextContainer:container];
    
    UIEdgeInsets inset = UIEdgeInsetsZero;
    inset.top =  txVi.bounds.size.height / 2 - textRect.size.height / 2;
    inset.left = txVi.bounds.size.width / 2 - textRect.size.width / 2;
    
    txVi.textContainerInset = inset;
}

#pragma mark - Add Items
- (void)addItemsForArray{
    NSUInteger n = arrMicForUse.count + NR_ITEM_TO_ADD;
    if (n > arrayWithQuotes.count) {
        n = arrayWithQuotes.count;
    }
    for (NSUInteger i = arrMicForUse.count; i<n; i++) {
        [arrMicForUse addObject:[arrayWithQuotes objectAtIndex:i]];
    }
}

#pragma maark - TEXTVIEW Delegate
- (void)textViewDidBeginEditing:(UITextView *)textView{
    self.tableWithMyQuotes.userInteractionEnabled = NO;
    textViewWithMyQuote.text = ((openForEdit)?textViewWithMyQuote.text:@"");
    textViewWithMyQuote.font = [UIFont fontWithName:FONT_DefoItalic size:((IS_IPAD)?20.0:15.0)];
    add.alpha = 0.0;
    add.hidden = NO;
    [UIView animateWithDuration:0.3 animations:^{
        add.alpha = 1.0;
    }];
    
}

- (void)textViewDidChange:(UITextView *)textView {
    CGRect line = [textView caretRectForPosition:
                   textView.selectedTextRange.start];
    CGFloat overflow = line.origin.y + line.size.height
    - ( textView.contentOffset.y + textView.bounds.size.height
       - textView.contentInset.bottom - textView.contentInset.top );
    if ( overflow > 0 ) {
        // We are at the bottom of the visible text and introduced a line feed, scroll down (iOS 7 does not do it)
        // Scroll caret to visible area
        CGPoint offset = textView.contentOffset;
        offset.y += overflow + 7; // leave 7 pixels margin
        // Cannot animate with setContentOffset:animated: or caret will not appear
        [UIView animateWithDuration:.2 animations:^{
            [textView setContentOffset:offset];
        }];
    }
}

#pragma mark - Add Text
- (IBAction)addMyQuote:(id)sender {
    [textViewWithMyQuote resignFirstResponder];
    BOOL b ;
    if (textViewWithMyQuote.text.length != 0) {
    if (openForEdit) {
        b = [storageDB editMyQuote:self->textViewWithMyQuote.text forId:idQuoteSelectForEdit];
    }
    else
    {
        b = [storageDB addToMyCite:[usrDef objectForKey:Author] withQoute:textViewWithMyQuote.text];
    }
    }else{
        b = NO;
    }
    if (!b) {
        NSLog(@"este bine = (%d)",b);
    }
    
    openForEdit = NO;
    add.hidden = YES;
    // TODO: add to table view
    [arrayWithQuotes removeAllObjects];
    [arrMicForUse removeAllObjects];
    arrayWithQuotes = [storageDB getMyQuotes];
    [self addItemsForArray];
    [self.tableWithMyQuotes reloadData];
    textViewWithMyQuote.text = @"Type Your Thoughts...";
    textViewWithMyQuote.font = [UIFont fontWithName:FONT_DefoItalic size:((IS_IPAD)?30.0:20.0)];
    self.tableWithMyQuotes.userInteractionEnabled = YES;
}

#pragma mark - EDIT Button

- (IBAction)editQuoteAction:(id)sender event:(id)event {

    [self.view setUserInteractionEnabled:NO];
    openForEdit = YES;
    
    CGPoint touchPosition = [[[event allTouches] anyObject] locationInView:self.tableWithMyQuotes];
    NSIndexPath *indexPath = [self.tableWithMyQuotes indexPathForRowAtPoint:touchPosition];
    idQuoteSelectForEdit = [NSNumber numberWithInteger:indexPath.row+1];
    NSDictionary *dict = [arrMicForUse objectAtIndex:indexPath.row];
    textViewWithMyQuote.text = [dict objectForKey:@"quote"];
    [textViewWithMyQuote becomeFirstResponder];
    [self.view setUserInteractionEnabled:YES];
}

#pragma mark - Alert Delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // the user clicked OK
    if (buttonIndex == 0) {
        [usrDef setObject:[NSString stringWithFormat:@"- %@",[[UIDevice currentDevice] name]] forKey:Author];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        NSString *name = [NSString stringWithFormat:@"- %@",[alertView textFieldAtIndex:0].text];
        [usrDef setObject:name forKey:Author];
    }
}

@end
