//
//  PhotoViewController.h
//  Quote
//
//  Created by Timotin Vanea on 1/9/14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoViewController : UIViewController<UIScrollViewDelegate, UIImagePickerControllerDelegate,UITextViewDelegate,UINavigationControllerDelegate,UIPopoverControllerDelegate>{

    IBOutlet UIImageView *imageShadowText;
    IBOutlet UIImageView *imageToShare;
    
    IBOutlet UIView *viewWithText;
    IBOutlet UIView *viewPhotoButtons;
    IBOutlet UIImageView *imageShadow;
    BOOL sliderTerminateAction;
    
    IBOutlet UIView *viewForLabelHidden;
    IBOutlet UILabel *labelSize;
    double heightPhoto, widthPhoto;
    IBOutlet UIView *viewResizeImage;
    
    IBOutlet UISlider *sliderForSize;
    
    IBOutlet UIView *view1;
    BOOL isPortrait;
    IBOutlet UIButton *buttonAddPhoto;
    IBOutlet UISegmentedControl *segmentControlPreview;
}

@property (readwrite, retain) NSString *editedQuote;
@property (readwrite, retain) NSString *editeCite;
@property (readwrite, retain) NSString *strFont;
@property (readwrite, retain) NSString *strNameCite;

@property(nonatomic, strong) IBOutlet UIImageView *backgroundPhoto;
@property (strong, nonatomic) IBOutlet UIView *viewForAllContent;
@property(nonatomic, strong) IBOutlet UIImageView *backgroundPhotoWithImageEffects;

- (IBAction)addPhotoAction:(id)sender;
- (IBAction)setOrientationAction:(id)sender;
- (IBAction)addViewChoosePathToPhoto:(id)sender;
- (IBAction)sliderTuchDown:(id)sender;
- (IBAction)sliderUpInside:(id)sender;
-(IBAction)valueChangedForSize:(UISlider*)sender;
@end
