//
//  PlistColor.m
//  Quote
//
//  Created by Timotin Vanea on 2/6/14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

#import "PlistColor.h"

@implementation PlistColor
static PlistColor  *sharedMyManager = nil;

+ (id)sharedColor {
    @synchronized(self) {
        if(sharedMyManager == nil)
            sharedMyManager = [[super allocWithZone:NULL] init];
    }
    return sharedMyManager;
}

-(id) init {
    self = [super init];
    if (self != nil) {
		[self conectToPlist];
    }
    return self;
}

- (void)conectToPlist{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"color" ofType:@"plist"];
    dictFromPlist = [[NSDictionary alloc] initWithContentsOfFile:path];
}

- (NSDictionary*)getDictWithColorsForID:(int8_t)colorId
{
    NSDictionary *dict = [NSDictionary dictionary];
    dict = [dictFromPlist objectForKey:[NSString stringWithFormat:@"%d",colorId]];
    return  dict;
}

@end
