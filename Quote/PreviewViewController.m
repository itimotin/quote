//
//  PreviewViewController.m
//  Quote
//
//  Created by Timotin Vanea on 1/20/14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

#import "PreviewViewController.h"
#import "SHKFacebook.h"
#import "SHKVkontakte.h"
#import "SHKTwitter.h"
#import "SHKMail.h"
#import "SHKDropbox.h"
#import "SHKGooglePlus.h"
#import "SHKFlickr.h"
#import "SHKTumblr.h"
#import "SHK.h"
#import "SHKItem.h"
#import "SHKSharer.h"

#import <AssetsLibrary/AssetsLibrary.h>

@interface PreviewViewController (){
    double scale;
    UIImage *screenshot;
    UIBarButtonItem *shareButton;
}

@end

@implementation PreviewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self addButtonToNavigationBar];
    labelQuote = [[UILabel alloc ] init];
    [viewWithAll addSubview:labelQuote];
    [self setTextViewQuote];
    [self situationOnView];
    [self arangementOnView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    viewWithButtonsShare.hidden = YES;
    viewWithAll.transform = CGAffineTransformMakeScale(1., 1.);
    UIGraphicsBeginImageContextWithOptions(viewWithAll.frame.size, NO, 0.0);
    [viewWithAll.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    screenshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    viewWithAll.transform = CGAffineTransformMakeScale(scale, scale);
    viewWithButtonsShare.hidden = NO;

}
- (void)arangementOnView{
    
    if (isIOS7orHigher()) {
        if (IS_IPAD) {
            
        }else if (isFourInch){
            
        }else{
            
        }
        
    }else{
        viewWithButtonsShare.frame = CGRectMake(WIDTH_DEVICE, -70, WIDTH_DEVICE/2, HEIGHT_DEVICE + 50);
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
     [[self navigationController] setNavigationBarHidden:NO animated:YES];
    NSDictionary *colorDict = [dictColor getDictWithColorsForID:[[usrDef objectForKey:@"color"] integerValue]];
    self.view.backgroundColor = UIColorFromRGB([[colorDict objectForKey:BGColor] integerValue]);
    if (!isIOS7orHigher()) {
        [[self navigationController] navigationBar].tintColor = UIColorFromRGB([[colorDict objectForKey:IMG_BGColor] integerValue]);
    }
}

- (void)addButtonToNavigationBar{
    shareButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"share"] style:UIBarButtonItemStylePlain target:self action:@selector(showSharing:)];

    self.navigationItem.rightBarButtonItem = shareButton;
    self.title = @"Sharing";
}

- (void)setTextViewQuote{
    labelQuote.text = [NSString stringWithFormat:@"\"%@\" \n  %@",self.editedQuote, self.editeCite];
    labelQuote.textColor = [UIColor whiteColor];
    labelQuote.font = [UIFont fontWithName:self.strFont size:self.fontSize];
}

-(void)settingsForLabelWhithSize:(CGSize)sizeLabel{
    labelQuote.frame = CGRectMake(0, 0, sizeLabel.width*0.8, self.selectedSize.height/2);
 
    labelQuote.numberOfLines = 0;
    [labelQuote sizeToFit];
    labelQuote.textAlignment = NSTextAlignmentCenter;
    labelQuote.center = CGPointMake(self.selectedSize.width/2, self.selectedSize.height- labelQuote.frame.size.height*0.7);
    imageShadowForText.frame = CGRectMake(labelQuote.frame.origin.x-100, labelQuote.frame.origin.y-50, labelQuote.frame.size.width+200, labelQuote.frame.size.height+100);
    imageShadowForText.center = labelQuote.center;
    
    labelQuote.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.1 alpha:0.015];
    imageShadowForText.layer.cornerRadius = 50;
    labelQuote.layer.cornerRadius = 25;

    if (self.editedQuote.length == 0 ) {
        imageShadowForText.hidden = YES;
    }
}

- (void)situationOnView{
    imageCenter.image = self.image;
    if (self.editedQuote.length > 0) {
        background.image = self.image;
        backgroundEffect.image = self.imageEffect;
    }
    viewWithAll.frame  = CGRectMake(0, 0, self.selectedSize.width, self.selectedSize.height);
    
    background.contentMode = backgroundEffect.contentMode = UIViewContentModeScaleAspectFill;
    
    CGSize sizePh = [self aspectScaledImageSizeForImageView:self.selectedSize image:imageCenter.image];

    viewCenterImage.frame = imageCenter.frame = CGRectMake(0, 0, sizePh.width, sizePh.height);
    
    viewCenterImage.center = CGPointMake(viewWithAll.frame.size.width/2, viewWithAll.frame.size.height/2);
    
    imageCenter.center = CGPointMake(viewCenterImage.frame.size.width/2, viewCenterImage.frame.size.height/2);

    [self settingsForLabelWhithSize:viewWithAll.frame.size];
    
    viewWithAll.center = CGPointMake(WIDTH_DEVICE/2, HEIGHT_DEVICE/2);
    
    scale = WIDTH_DEVICE/self.selectedSize.width;
    
    viewWithAll.transform = CGAffineTransformMakeScale(scale, scale);
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showSharing:(id)sender {
    UIBarButtonItem *button = (UIBarButtonItem*)sender;
    if (viewWithButtonsShare.center.x < WIDTH_DEVICE) {
        [self hiddenViewWithButtons];
    }else{
        [button setImage:[UIImage imageNamed:@"close"]];
        [self showViewWithButtons];
    }

}

-(void)showViewWithButtons {

    [UIView animateWithDuration:.5 animations:^{
        
        viewWithButtonsShare.center = CGPointMake(WIDTH_DEVICE-viewWithButtonsShare.frame.size.width/2, HEIGHT_DEVICE/2-((isIOS7orHigher())?0:65));
    } completion:^(BOOL finished){
    }];
}

- (CGSize) aspectScaledImageSizeForImageView:(CGSize)size image:(UIImage *)im {
    
    float x,y;
    float a,b;
    x = size.width;
    y = size.height;
    a = im.size.width;
    b = im.size.height;
    
    if ( x == a && y == b ) {           // image fits exactly, no scaling required
        //Nu trebu de schimbat e exact cit ne trebu.
    }
    else if ( x > a && y > b ) {         // image fits completely within the imageview frame
//        if ( x-a > y-b ) {              // image height is limiting factor, scale by height
//            a = y/b * a;
//            b = y;
//        } else {
//            b = x/a * b;                // image width is limiting factor, scale by width
//            a = x;
//        }
        //Nu trebu de schimbat sau de facut fit deoarece se strica imaginea.
    }
    else if ( x < a && y < b ) {        // image is wider and taller than image view
        if ( a - x > b - y ) {          // height is limiting factor, scale by height
            //E faorte bine calculat
            a = y/b * a;
            b = y;
        } else {                        // width is limiting factor, scale by width
            b = x/a * b;
            a = x;
        }
    }
    else if ( x < a && y > b ) {        // image is wider than view, scale by width
        b = x/a * b;
        a = x;
    }
    else if ( x > a && y < b ) {        // image is taller than view, scale by height
        a = y/b * a;
        b = y;
    }
    else if ( x == a ) {
        a = y/b * a;
        b = y;
    } else if ( y == b ) {
        b = x/a * b;
        a = x;
    }
    return CGSizeMake(a,b);
    
}

- (void)hiddenViewWithButtons {
    [shareButton setImage:[UIImage imageNamed:@"share"]];
    [UIView animateWithDuration:.5 animations:^{
        viewWithButtonsShare.center = CGPointMake(WIDTH_DEVICE+viewWithButtonsShare.frame.size.width/2, HEIGHT_DEVICE/2-((isIOS7orHigher())?0:65));
    } completion:^(BOOL finished){
     
    }];
}
- (IBAction)shareTo:(id)sender {
    [self hiddenViewWithButtons];
    NSInteger tag = ((UIButton*)sender).tag;
    SHKItem *item=[[SHKItem alloc]init];
    item.shareType=SHKShareTypeImage;
    item.title=@"FOOD FOR MIND";
    item.image = screenshot;
    switch (tag) {
        case 1:
            //facebook
            [SHKFacebook shareItem:item];
            break;
        case 2:
            //library
            UIImageWriteToSavedPhotosAlbum(screenshot, nil, nil, nil);
            break;
        case 3:
            //twit
            [SHKTwitter shareItem:item];
            break;
        case 4:
            //tumblr
            [SHKTumblr shareItem:item];
            break;
        case 5:
            //flickr
            [SHKFlickr shareItem:item];
            break;
        case 6:
            //mail
            [SHKMail shareItem:item];
            break;
        case 7:
            //vk
            [SHKVkontakte shareItem:item];
            break;
        case 8:
            //dropbox
            [SHKDropbox shareItem:item];
            break;
        case 9:
            //g+
            [SHKGooglePlus shareItem:item];
            break;
            
            
        default:
            break;
    }
    
}

@end
