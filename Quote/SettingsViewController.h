//
//  SettingsViewController.h
//  Quote
//
//  Created by Timotin Vanea on 1/8/14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface SettingsViewController : UIViewController <UITextFieldDelegate>{
    enum{
        kTagButtonsView=1000,
        kTagButtonSetTime,
        kTagNotificationSwitch,
        kTagSavePhotos
    };

    IBOutlet UIButton *buttonRefreshReadedQuotes;
    IBOutlet UILabel *labelNotif;
    IBOutlet UIView *viewButtons;
    IBOutlet UITextField *uitextFieldWithName;
    IBOutlet UILabel *lableInstructionName;
}

@property (strong, nonatomic) IBOutlet UISwitch *notificationSwitch;

- (IBAction)switchAction:(id)sender;
- (IBAction)refreshQuotes:(id)sender;


@end
