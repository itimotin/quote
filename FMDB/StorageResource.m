#import "StorageResource.h"

@implementation StorageResource

static StorageResource  *sharedMyManager = nil;

+ (id)sharedManager {
    @synchronized(self) {
        if(sharedMyManager == nil)
            sharedMyManager = [[super allocWithZone:NULL] init];
    }
    return sharedMyManager;
}

#pragma mark - ADD to Quotes
-(void)addToDBCite:(NSString *)strCite withQoute:(NSString *)strQoute withInterpretation:(NSString*)strInterpretation{
    NSUInteger count = [db intForQuery:@"SELECT COUNT(id) FROM quotes"];
//    NSString *queryString = [NSString stringWithFormat:@"insert into quotes VALUES(%ld,%@,%@);",(count+1),strCite,strQoute];
    BOOL b=[db executeUpdate:@"insert into quotes VALUES(?,?,?,?,?);",[NSNumber numberWithInteger:(count+1)],strCite,strQoute,[NSNumber numberWithInteger:0],strInterpretation];
    NSLog(@"%ld-%d",(long)count,b);
}


- (NSDictionary*)dictForID:(NSString *)idQuote{
    FMResultSet *rs = [db executeQuery:@"select cite, quote, interpretation from quotes where id = ?",idQuote];
    NSDictionary *dict = [[NSDictionary alloc] init];
    while ([rs next]) {
        dict = [rs resultDict];
    }
    return dict;
}

#pragma mark - ALL
- (NSInteger)giveNumberOfQuotes{
    return [db intForQuery:@"select count(id) from quotes;"];
}


- (NSMutableArray*) getAllQuotes{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    FMResultSet *rs = [db executeQuery:@"select * from quotes;"];
    
    while ([rs next]) {
        [arr addObject:[rs resultDict]];
    }
    
    return arr;
}

- (NSMutableArray*)getAllForType:(NSNumber*)type {
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    FMResultSet *rs = [db executeQuery:@"select * from quotes where category = ?;", type];
    
    while ([rs next]) {
        [arr addObject:[rs resultDict]];
    }
    
    return arr;
}

#pragma mark - FAVORITES
- (NSMutableArray*) getAllFavorites{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    FMResultSet *rs = [db executeQuery:@"select * from likes;"];
    
    while ([rs next]) {
        [arr addObject:[rs resultDict]];
    }
    
    return arr;
}

- (BOOL)addToFavoritesQuoteWithId:(NSString*)idQuote
{
    
    BOOL b = [db executeUpdate:@" UPDATE `quotes` SET `liked` = ? WHERE  `rowid` = ?;", @(1),idQuote];
    
    FMResultSet *rs = [db executeQuery:@"select cite, quote, interpretation from quotes where id = ?",idQuote];
    NSDictionary *dict = [[NSDictionary alloc] init];
    
    while ([rs next]) {
        dict = [rs resultDict];
    }
    
    BOOL z=[db executeUpdate:@"insert into likes VALUES(?,?,?,?,?,?,?);", idQuote, [dict objectForKey:@"quote"],[dict objectForKey:@"cite"], [dict objectForKey:@"interpretation"],@(1),@(0),@(1)];
    return (b&&z);
}

- (BOOL)removeFromFavoritesQuoteWithId:(NSString*)idQuote
{
    
    BOOL b = [db executeUpdate:@" UPDATE `quotes` SET `liked` = ? WHERE  `rowid` = ?;", @(0),idQuote];
    BOOL z=[db executeUpdate:@"DELETE FROM likes WHERE id = ?;", idQuote];
    
    return (z&&b);
}

#pragma mark - MY
- (NSMutableArray*) getMyQuotes
{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    FMResultSet *rs = [db executeQuery:@"select * from my;"];
    
    while ([rs next]) {
        [arr addObject:[rs resultDict]];
    }
    
    return arr;
}

-(BOOL)addToMyCite:(NSString *)strCite withQoute:(NSString *)strQoute
{
    NSUInteger count = [db intForQuery:@"SELECT COUNT(id) FROM my"];

    BOOL b=[db executeUpdate:@"insert into my VALUES(?,?,?);",[NSNumber numberWithInteger:(count+1)],strQoute,strCite];
    
    return b;
}

- (BOOL)editMyQuote:(NSString *)strQoute forId:(NSNumber*)idQuote
{
    BOOL b = [db executeUpdate:@" UPDATE `my` SET `quote` = ? WHERE  `id` = ?;", strQoute, idQuote];
    return b;
}

#pragma mark - 

- (NSMutableArray*) giveIndexesOfQuotes{
    NSMutableArray *array = [NSMutableArray array];
    FMResultSet *rs = [db executeQuery:@"SELECT id FROM quotes ORDER BY id ASC;"];
    while ([rs next]){
        [array addObject:[rs objectForColumnName:@"id"]];
    }
    return  array;
}

- (NSString*)giveFormatFromDate:(NSDate*)currentDate{
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:currentDate];
    NSInteger day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    NSLog(@"%ld-%02ld-%02ld", (long)year, (long)month, (long)day);
    return [NSString stringWithFormat:@"%ld-%02ld-%02ld", (long)year, (long)month, (long)day];
}

- (NSDate*)giveDateAfterYearOfDate:(NSDate*)today{
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:today];
    NSInteger day = [components day];
    NSInteger year = [components year];
    
    [components setYear:year+1];
    [components setDay:day+1];
    
    return [calendar dateFromComponents:components];
}

#pragma mark - Read
- (BOOL)addToReadQuoteWithId:(NSString*)idQuote
{
    BOOL b = [db executeUpdate:@" UPDATE `quotes` SET `readed` = ? WHERE  `rowid` = ?;",@(1),idQuote];
    return b;
}
- (BOOL)removeFromReadedQuoteWithId:(NSString*)idQuote
{
    
    BOOL b = [db executeUpdate:@" UPDATE `quotes` SET `readed` = ? WHERE  `rowid` = ?;",@(0),idQuote];
    return b;
}

- (NSMutableArray*)getAllForSearchedText:(NSString*)searchStr{
    NSString *search = [NSString stringWithFormat:@"select * from quotes WHERE quote LIKE '%%%@%%' OR cite LIKE '%%%@%%' OR interpretation LIKE '%%%@%%' ORDER BY id DESC;",searchStr,searchStr,searchStr];
    NSLog(@"%@",search);
    
    FMResultSet *rs = [db executeQuery:search];
    NSMutableArray *array = [NSMutableArray array];
    while ([rs next]){
        [array addObject:[rs resultDict]];
    }
    
    return array;
}

#pragma mark - REFRESH READED 
- (BOOL)refreshAllReaded {
    
    return [db executeUpdate:@" UPDATE `quotes` SET `readed` = 0 WHERE  `readed` = 1;"];
}

#pragma mark - INIT
-(id) init {
    self = [super init];
    if (self != nil) {
		[self checkAndInitDatabase];
    }
    return self;
}


- (void)checkAndInitDatabase{
    NSError *error = nil;
	NSString *path = [documentDirectoryPLUS
					  stringByAppendingPathComponent:@".512x512.gif"];
	if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:[[NSBundle mainBundle]pathForResource:@".512x512" ofType:@"gif"]]) {
            NSLog(@"exista asa File");
            [[NSFileManager defaultManager] copyItemAtPath:[[NSBundle mainBundle]pathForResource:@".512x512" ofType:@"gif"] toPath:path error:&error];
        }else{
            NSLog(@"nu exista asa fisier pentru path = %@",[[NSBundle mainBundle]pathForResource:@".512x512" ofType:@"gif"]);
        }
        
        
        //        [[NSFileManager defaultManager] createFileAtPath:[documentsDirectory stringByAppendingPathComponent:@".512x512.png"] contents:nil attributes:nil];
	}
	NSLog(@"path is %@",path);
	db = [[FMDatabase alloc] initWithPath:path];
	[db setLogsErrors:TRUE];
	if (![db open]) {
        
	}
    else
    {
        NSLog(@"\n\n ==>DB Open...\n DB PATH = %@",path);
        if (![db tableExists:@"quotes"])
        {
            [db executeUpdate:@" CREATE TABLE 'quotes' ('id' INTEGER,'cite' TEXT,'quote' TEXT PRIMARY KEY NOT NULL,'category' INTEGER,'interpretation' TEXT, 'liked' BOOL NOT NULL  DEFAULT 0, 'readed' BOOL NOT NULL  DEFAULT 0);",nil];
        }
        
        if (![db tableExists:@"likes"])
        {
            [db executeUpdate:@" CREATE TABLE  'likes' ('id' INTEGER PRIMARY KEY NOT NULL,'quote' TEXT, 'cite' TEXT, 'interpretation' TEXT, 'liked' BOOL NOT NULL  DEFAULT 1,'category' INTEGER,'readed' BOOL NOT NULL  DEFAULT 0);",nil];
        }
        
        if (![db tableExists:@"my"])
        {
            [db executeUpdate:@" CREATE TABLE  'my' ('id' INTEGER, 'quote' TEXT PRIMARY KEY NOT NULL, 'cite' TEXT);",nil];
        }
    }
}

@end
