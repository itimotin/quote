
#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import <CoreLocation/CoreLocation.h>


@interface StorageResource : NSObject {
    FMDatabase *db;
    NSString *tableName;
}
+ (id)sharedManager;

-(void)addToDBCite:(NSString *)strCite withQoute:(NSString *)strQoute withInterpretation:(NSString*)strInterpretation;
- (NSInteger)giveNumberOfQuotes;

- (NSMutableArray*) giveIndexesOfQuotes;
- (NSDictionary*)dictForID:(NSString*)idQuote;
- (NSString*)giveFormatFromDate:(NSDate*)currentDate;

- (NSMutableArray*) getAllQuotes;

#pragma mark - MY
- (NSMutableArray*) getMyQuotes;
- (BOOL)addToMyCite:(NSString *)strCite withQoute:(NSString *)strQoute;
- (BOOL)editMyQuote:(NSString *)strQoute forId:(NSNumber*)idQuote;

- (NSMutableArray*) getAllFavorites;

- (BOOL)addToFavoritesQuoteWithId:(NSString*)idQuote;
- (BOOL)removeFromFavoritesQuoteWithId:(NSString*)idQuote;

- (BOOL)addToReadQuoteWithId:(NSString*)idQuote;
- (BOOL)removeFromReadedQuoteWithId:(NSString*)idQuote;

- (NSMutableArray*)getAllForSearchedText:(NSString*)searchStr;

- (NSMutableArray*)getAllForType:(NSNumber*)type;

- (BOOL)refreshAllReaded;

@end
